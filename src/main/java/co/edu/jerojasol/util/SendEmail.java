package co.edu.jerojasol.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import co.edu.jerojasol.entities.Marcador;
import co.edu.jerojasol.entities.Partido;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class SendEmail {

	private final String USER_NAME = "pollamundialstt2014@gmail.com"; // GMail
																		// user
																		// name
																		// (just
																		// the
																		// part
																		// before
																		// "@gmail.com")
	private final String PASSWORD = "Santiag00607"; // GMail password

	public void sendEmail() {
	}

	public void sendFromGMail(Partido partido, List<Marcador> marcadores) {

		String from = USER_NAME;
		String pass = PASSWORD;
		String[] to = new String[marcadores.size()];

		int contador = 0;
		for (Marcador marcador : marcadores) {
			to[contador] = marcador.getIdusuario().getCorreo();
			contador++;
		}

		Properties props = System.getProperties();
		String host = "smtp.gmail.com";
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", from);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props);
		MimeMessage message = new MimeMessage(session);

		try {

			String subject = "Marcadores Partido "
					+ partido.getIdequipoUno().getNombre() + " - "
					+ partido.getIdequipoDos().getNombre() + ", "
					+ partido.getIdfase().getNombre();

			Configuration config = new Configuration();
			config.setTemplateLoader(new ClassTemplateLoader(this.getClass(),
					"/"));
			Template template = config
					.getTemplate("co/edu/jerojasol/templates/informeMarcadores.tpl");

			Map<String, Object> model = new HashMap<String, Object>();
			model.put("partido", partido);
			model.put("marcadores", marcadores);

			Writer out = new StringWriter();
			template.process(model, out);

			message.setFrom(new InternetAddress(from));
			InternetAddress[] toAddress = new InternetAddress[to.length];

			// To get the array of addresses
			for (int i = 0; i < to.length; i++) {
				toAddress[i] = new InternetAddress(to[i]);
			}

			for (int i = 0; i < toAddress.length; i++) {
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			message.setSubject(subject);
			message.setContent(out.toString(), "text/html; charset=utf-8");
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (AddressException ae) {
			ae.printStackTrace();
		} catch (MessagingException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}

}
