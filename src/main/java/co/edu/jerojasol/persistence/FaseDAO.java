package co.edu.jerojasol.persistence;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.edu.jerojasol.entities.Equipo;
import co.edu.jerojasol.entities.Fase;

@Repository
public class FaseDAO {

	@Autowired
    private SessionFactory sessionFactory;
	
	/**
     * Get Hibernate Session Factory
     *
     * @return SessionFactory - Hibernate Session Factory
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Set Hibernate Session Factory
     *
     * @param sessionFactory SessionFactory - Hibernate Session Factory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * Get customer List
     *
     * @return List - customer list
     */
    public List<Fase> getFases() {
    	
    	String query = "SELECT f FROM Fase f WHERE f.estado = 'A' order by f.prioridad";
    	
        List list = getSessionFactory().getCurrentSession().createQuery(query).list();
        return list;
        
    }
}
