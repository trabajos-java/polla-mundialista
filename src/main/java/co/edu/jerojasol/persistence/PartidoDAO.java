package co.edu.jerojasol.persistence;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.edu.jerojasol.entities.Fase;
import co.edu.jerojasol.entities.Partido;
import co.edu.jerojasol.entities.Usuario;

@Repository
public class PartidoDAO {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Get Hibernate Session Factory
	 * 
	 * @return SessionFactory - Hibernate Session Factory
	 */
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	/**
	 * Set Hibernate Session Factory
	 * 
	 * @param sessionFactory
	 *            SessionFactory - Hibernate Session Factory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Get customer List
	 *
	 * @return List - customer list
	 */
	public List<Partido> getPartidos() {

		String query = "SELECT p FROM Partido p";

		List list = getSessionFactory().getCurrentSession().createQuery(query)
				.list();
		return list;

	}

	public List<Partido> getPartidosPorFase(Integer idFase) {

		final Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Partido.class);

		criteria.add(Restrictions.eq("idfase.idfase", idFase));

		List<Partido> list = criteria.list();

		return list;

	}

	public List<Partido> getPartidosPorFecha() {
		String query = "SELECT p FROM Partido p WHERE estado = 'A' order by p.fecha";

		List list = getSessionFactory().getCurrentSession().createQuery(query)
				.list();
		return list;
	}

	public Partido consultarPartido(Integer idPartido) {

		Partido resultado = null;

		final Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Partido.class);

		criteria.add(Restrictions.eq("idpartido", idPartido));

		List<Partido> list = criteria.list();

		if (list != null && list.size() > 0) {
			resultado = list.get(0);
		}
		return resultado;

	}

	public Partido actualizarPartido(Partido partido) {
		Session session = getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(partido);
			transaction.commit();
			System.out.println("Records inserted sucessessfully");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			partido = null;
		} finally {
			session.close();
		}
		return partido;
	}

}
