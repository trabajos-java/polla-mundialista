package co.edu.jerojasol.persistence;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.edu.jerojasol.entities.Marcador;
import co.edu.jerojasol.entities.Usuario;

@Repository
public class MarcadorDAO {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Get Hibernate Session Factory
	 * 
	 * @return SessionFactory - Hibernate Session Factory
	 */
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	/**
	 * Set Hibernate Session Factory
	 * 
	 * @param sessionFactory
	 *            SessionFactory - Hibernate Session Factory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Marcador createMarcador(Marcador marcador) {
		Session session = getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(marcador);
			transaction.commit();
			System.out.println("Records inserted sucessessfully");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			marcador = null;
		} finally {
			session.close();
		}
		return marcador;
	}
	
	public List<Marcador> consultarMarcadoresPorUsuario(Integer idUsuario){
		
		final Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Marcador.class);

		criteria.add(Restrictions.eq("idusuario.idusuario", idUsuario));

		List<Marcador> list = criteria.list();
		return list;
	}
	
	public List<Marcador> consultarMarcadoresPorPartido(Integer idPartido){
		
		final Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Marcador.class);
		
		criteria.add(Restrictions.eq("idpartido.idpartido", idPartido));
		
		List<Marcador> list = criteria.list();
		return list;
	}
	
	public List<Marcador> consultarMarcadoresCerradosPorUsuario(Integer idUsuario){
		
		final Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Marcador.class);
		
		criteria.add(Restrictions.eq("idusuario.idusuario", idUsuario));
//		criteria.setFetchMode("idpartido", FetchMode.JOIN);
//		criteria.add(Restrictions.eq("idpartido.estado", "C"));
		criteria.add(Restrictions.isNotNull("marcadorUno"));
		
		List<Marcador> list = criteria.list();
		return list;
	}

	public Marcador actualizarMarcador(Marcador marcador){
		Session session = getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(marcador);
			transaction.commit();
			System.out.println("Records inserted sucessessfully");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
			marcador = null;
		} finally {
			session.close();
		}
		return marcador;
	}
	
}
