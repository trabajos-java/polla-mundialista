package co.edu.jerojasol.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.jerojasol.entities.Fase;
import co.edu.jerojasol.entities.Partido;
import co.edu.jerojasol.entities.PosicionEquipo;
import co.edu.jerojasol.persistence.FaseDAO;

@Service("FaseService")
@Transactional(readOnly = true)
public class FaseService {

	@Autowired
	FaseDAO faseDAO;

	public List<Fase> getFases() {

		List<Fase> fases = faseDAO.getFases();

		for (Fase fase : fases) {
			for (Partido partido : fase.getPartidoList()) {
				System.out.println(partido.getIdequipoUno().getNombre() + " vs " + partido.getIdequipoDos().getNombre());
				if (partido.getResultadoUno() == null) {
					partido.setResultadoUno(new Integer(0));
				}
				if (partido.getResultadoDos() == null) {
					partido.setResultadoDos(new Integer(0));
				}
			}
		}

		return fases;
	}

	public FaseDAO getFaseDAO() {
		return faseDAO;
	}

	public void setFaseDAO(FaseDAO faseDAO) {
		this.faseDAO = faseDAO;
	}

	public List<PosicionEquipo> obtenerPosicionEquipos(Fase fase) {

		Map<Integer, PosicionEquipo> posicionEquipos = new HashMap<Integer, PosicionEquipo>();

		for (Partido partido : fase.getPartidoList()) {

			PosicionEquipo equipo1 = posicionEquipos.get(partido.getIdequipoUno().getIdequipo());
			PosicionEquipo equipo2 = posicionEquipos.get(partido.getIdequipoDos().getIdequipo());

			if (equipo1 == null) {
				equipo1 = new PosicionEquipo(partido.getIdequipoUno());
			}

			if (equipo2 == null) {
				equipo2 = new PosicionEquipo(partido.getIdequipoDos());
			}
			
			if (partido.getEstado().equals("C")) {

				equipo1.setPartidosJugados(equipo1.getPartidosJugados() + 1);
				equipo2.setPartidosJugados(equipo2.getPartidosJugados() + 1);

				equipo1.setGolesFavor(equipo1.getGolesFavor() + partido.getResultadoUno());
				equipo2.setGolesFavor(equipo2.getGolesFavor() + partido.getResultadoDos());

				equipo1.setGolesContra(equipo1.getGolesContra() + partido.getResultadoDos());
				equipo2.setGolesContra(equipo2.getGolesContra() + partido.getResultadoUno());

				equipo1.setDiferenciaGol(equipo1.getGolesFavor() - equipo1.getGolesContra());
				equipo2.setDiferenciaGol(equipo2.getGolesFavor() - equipo2.getGolesContra());

				if (partido.getResultadoUno().intValue() == partido.getResultadoDos().intValue()) {
					equipo1.setPartidosEmpatados(equipo1.getPartidosEmpatados() + 1);
					equipo2.setPartidosEmpatados(equipo2.getPartidosEmpatados() + 1);

					equipo1.setPuntos(equipo1.getPuntos() + 1);
					equipo2.setPuntos(equipo2.getPuntos() + 1);
				} else if (partido.getResultadoUno().intValue() > partido.getResultadoDos().intValue()) {
					equipo1.setPartidosGanados(equipo1.getPartidosGanados() + 1);
					equipo2.setPartidosPerdidos(equipo1.getPartidosPerdidos() + 1);

					equipo1.setPuntos(equipo1.getPuntos() + 3);
				} else {
					equipo2.setPartidosGanados(equipo2.getPartidosGanados() + 1);
					equipo1.setPartidosPerdidos(equipo1.getPartidosPerdidos() + 1);

					equipo2.setPuntos(equipo2.getPuntos() + 3);
				}
				
			}
			posicionEquipos.put(equipo1.getEquipo().getIdequipo(), equipo1);
			posicionEquipos.put(equipo2.getEquipo().getIdequipo(), equipo2);
		}

		List<PosicionEquipo> resultados = new ArrayList<PosicionEquipo>();
		Iterator it = posicionEquipos.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry e = (Map.Entry) it.next();
			resultados.add((PosicionEquipo) e.getValue());
		}
		ordenarEquipos(resultados);
		return resultados;
	}

	public void ordenarEquipos(List<PosicionEquipo> equipos) {

		for (int i = 0; i < equipos.size() - 1; i++) {
			for (int j = i+1; j < equipos.size(); j++) {

				PosicionEquipo equipo1 = equipos.get(i);
				PosicionEquipo equipo2 = equipos.get(j);

				boolean cambiarPosicion = false;
				if (equipo1.getPuntos() < equipo2.getPuntos()) {
					cambiarPosicion = true;
				} else if (equipo1.getPuntos() == equipo2.getPuntos()) {
					if (equipo1.getDiferenciaGol() < equipo2.getDiferenciaGol()) {
						cambiarPosicion = true;
					}
				}
				if (cambiarPosicion) {
					PosicionEquipo equipoTmp = equipo1;
					equipos.set(i, equipo2);
					equipos.set(j, equipoTmp);
				}
			}
		}

	}

}
