package co.edu.jerojasol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.jerojasol.entities.Marcador;
import co.edu.jerojasol.entities.Partido;
import co.edu.jerojasol.persistence.PartidoDAO;

@Service("PartidoService")
@Transactional(readOnly = true)
public class PartidoService {

	@Autowired
	PartidoDAO partidoDAO;

	public PartidoDAO getPartidoDAO() {
		return partidoDAO;
	}

	public void setPartidoDAO(PartidoDAO partidoDAO) {
		this.partidoDAO = partidoDAO;
	}
	
	public Partido consultarPartido(Integer idPartido) {
		return partidoDAO.consultarPartido(idPartido);
	}
	
	public Partido consultarPartidoConMarcadores(Integer idPartido) {
		Partido resultado = partidoDAO.consultarPartido(idPartido);
		
		System.out.println("Se encontro partido de equipos "+resultado.getIdequipoUno().getNombre()+" y "+resultado.getIdequipoDos().getNombre()+" para la fase "+resultado.getIdfase().getNombre());
		
		for (Marcador marcador : resultado.getMarcadorList()){
			System.out.println("Se encontro marcador "+marcador.getIdmarcador()+" para el usuario "+marcador.getIdusuario().getNombres());
		}
	
		return resultado;
	}
	
	public Partido consultarPartidoSinMarcadores(Integer idPartido) {
		Partido resultado = partidoDAO.consultarPartido(idPartido);
		
		if (resultado != null){
			System.out.println("Se encontro partido de equipos "+resultado.getIdequipoUno().getNombre()+" y "+resultado.getIdequipoDos().getNombre());
		}
		
		return resultado;
	}
	
	public List<Partido> getPartidosPorFase(Integer idFase){
		
		List<Partido> resultado = partidoDAO.getPartidosPorFase(idFase);
		
		for (Partido partido: resultado){
			System.out.println(partido.getIdequipoUno()+" vs "+partido.getIdequipoDos());
		}
		
		return resultado;
	}
	
	public Partido actualizarPartido(Partido partido){
		return partidoDAO.actualizarPartido(partido);
	}
	
	public List<Partido> obtenerPartidosPorFecha(){
		
		List<Partido> resultado = partidoDAO.getPartidosPorFecha();
		
		for (Partido partido: resultado){
			System.out.println(partido.getIdequipoUno()+" vs "+partido.getIdequipoDos());
		}
		
		return resultado;
	}
	
}
