package co.edu.jerojasol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.jerojasol.entities.Marcador;
import co.edu.jerojasol.persistence.MarcadorDAO;

@Service("MarcadorService")
@Transactional(readOnly = true)
public class MarcadorService {

	@Autowired
	MarcadorDAO marcadorDAO;

	public MarcadorDAO getMarcadorDAO() {
		return marcadorDAO;
	}

	public void setMarcadorDAO(MarcadorDAO marcadorDAO) {
		this.marcadorDAO = marcadorDAO;
	}
	
	public List<Marcador> consultarMarcadoresPorUsuario(Integer idUsuario){
		
		List<Marcador> resultado =marcadorDAO.consultarMarcadoresPorUsuario(idUsuario);
		
		for (Marcador marcador : resultado){
			System.out.println("Resultado usuario "+idUsuario+" para partido: "+marcador.getIdpartido()+" "+marcador.getMarcadorUno()+"-"+marcador.getMarcadorDos());
		}
		
		return resultado;
		
	}
	
	public List<Marcador> consultarMarcadoresPorPartido(Integer idPartido){
		
		List<Marcador> resultado =marcadorDAO.consultarMarcadoresPorPartido(idPartido);
		
		for (Marcador marcador : resultado){
			System.out.println("Resultado para partido: "+marcador.getIdpartido()+" "+marcador.getMarcadorUno()+"-"+marcador.getMarcadorDos());
		}
		
		return resultado;
		
	}
	
	public Marcador actualizarMarcador(Marcador marcador){
		return marcadorDAO.actualizarMarcador(marcador);
	}
	
}
