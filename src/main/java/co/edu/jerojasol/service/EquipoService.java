package co.edu.jerojasol.service;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.edu.jerojasol.entities.Equipo;
import co.edu.jerojasol.persistence.EquipoDAO;

/**
 *
 * Customer Service
 *
 * @author uday
 * @since 19 Nov 2013
 * @version 1.0.0
 *
 *
 */
@Service("EquipoService")
@Transactional(readOnly = true)
public class EquipoService {
 
    // CustomerDAO is injected...
    @Autowired
    EquipoDAO equipoDAO;
 
    /**
     * Add Customer
     *
     * @param  customer Customer
     */
//    @Transactional(readOnly = false)
//    public void addCustomer(Customer customer) {
//        getCustomerDAO().addCustomer(customer);
//    }
 
    /**
     * Delete Customer
     *
     * @param   customer  Customer
     */
//    @Transactional(readOnly = false)
//    public void deleteCustomer(Customer customer) {
//        getCustomerDAO().deleteCustomer(customer);
//    }
 
    /**
     * Update Customer
     *
     * @param customer  Customer
     */
//    @Transactional(readOnly = false)
//    public void updateCustomer(Customer customer) {
//        getCustomerDAO().updateCustomer(customer);
//    }
 
    /**
     * Get Customer
     *
     * @param  id int Customer Id
     */

//    public Customer getCustomerById(int id) {
//        return getCustomerDAO().getCustomerById(id);
//    }
 
    /**
     * Get Customer List
     *
     */

    public List<Equipo> getEqupios() {
        return getEquipoDAO().getEquipos();
    }
 
    /**
     * Get Customer DAO
     *
     * @return customerDAO - Customer DAO
     */
    public EquipoDAO getEquipoDAO() {
        return equipoDAO;
    }
 
    /**
     * Set Customer DAO
     *
     * @param  customerDAO - CustomerDAO
     */
    public void setEquipoDAO(EquipoDAO equipoDAO) {
        this.equipoDAO = equipoDAO;
    }
}