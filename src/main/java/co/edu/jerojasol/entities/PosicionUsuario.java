package co.edu.jerojasol.entities;

import java.io.Serializable;

public class PosicionUsuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private Usuario usuario;
	private int primeraFase;
	private int octavos;
	private int cuartos;
	private int semifinales;
	private int terceroCuarto;
	private int puntosFinal;
	private int puntosTotal;

	public PosicionUsuario(Usuario usuario) {
		super();
		this.usuario = usuario;
		primeraFase = 0;
		octavos = 0;
		cuartos = 0;
		semifinales = 0;
		terceroCuarto = 0;
		puntosFinal = 0;
		puntosTotal = 0;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public int getPrimeraFase() {
		return primeraFase;
	}

	public void setPrimeraFase(int primeraFase) {
		this.primeraFase = primeraFase;
	}

	public int getOctavos() {
		return octavos;
	}

	public void setOctavos(int octavos) {
		this.octavos = octavos;
	}

	public int getCuartos() {
		return cuartos;
	}

	public void setCuartos(int cuartos) {
		this.cuartos = cuartos;
	}

	public int getSemifinales() {
		return semifinales;
	}

	public void setSemifinales(int semifinales) {
		this.semifinales = semifinales;
	}

	public int getTerceroCuarto() {
		return terceroCuarto;
	}

	public void setTerceroCuarto(int terceroCuarto) {
		this.terceroCuarto = terceroCuarto;
	}

	public int getPuntosFinal() {
		return puntosFinal;
	}

	public void setPuntosFinal(int puntosFinal) {
		this.puntosFinal = puntosFinal;
	}

	public int getPuntosTotal() {
		return puntosTotal;
	}

	public void setPuntosTotal(int puntosTotal) {
		this.puntosTotal = puntosTotal;
	}

}
