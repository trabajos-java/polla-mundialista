/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.jerojasol.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DESARROLLO01
 */
@Entity
@Table(name = "marcador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Marcador.findAll", query = "SELECT m FROM Marcador m")})
public class Marcador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmarcador")
    private Integer idmarcador;
    @Column(name = "marcadorUno")
    private Integer marcadorUno;
    @Column(name = "marcadorDos")
    private Integer marcadorDos;
    @Basic(optional = false)
    @Column(name = "fechaModificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @JoinColumn(name = "idusuario", referencedColumnName = "idusuario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usuario idusuario;
    @JoinColumn(name = "idpartido", referencedColumnName = "idpartido")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Partido idpartido;

    public Marcador() {
    }

    public Marcador(Integer idmarcador) {
        this.idmarcador = idmarcador;
    }

    public Marcador(Integer idmarcador, Date fechaModificacion) {
        this.idmarcador = idmarcador;
        this.fechaModificacion = fechaModificacion;
    }

    public Integer getIdmarcador() {
        return idmarcador;
    }

    public void setIdmarcador(Integer idmarcador) {
        this.idmarcador = idmarcador;
    }

    public Integer getMarcadorUno() {
        return marcadorUno;
    }

    public void setMarcadorUno(Integer marcadorUno) {
        this.marcadorUno = marcadorUno;
    }

    public Integer getMarcadorDos() {
        return marcadorDos;
    }

    public void setMarcadorDos(Integer marcadorDos) {
        this.marcadorDos = marcadorDos;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    public Partido getIdpartido() {
        return idpartido;
    }

    public void setIdpartido(Partido idpartido) {
        this.idpartido = idpartido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmarcador != null ? idmarcador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Marcador)) {
            return false;
        }
        Marcador other = (Marcador) object;
        if ((this.idmarcador == null && other.idmarcador != null) || (this.idmarcador != null && !this.idmarcador.equals(other.idmarcador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pollamundilista.Marcador[ idmarcador=" + idmarcador + " ]";
    }
    
}
