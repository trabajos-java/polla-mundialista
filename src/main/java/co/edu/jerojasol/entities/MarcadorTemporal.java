package co.edu.jerojasol.entities;

public class MarcadorTemporal {

	private Integer idmarcador;
	private String marcadorUno;
	private String marcadorDos;

	public Integer getIdmarcador() {
		return idmarcador;
	}

	public void setIdmarcador(Integer idmarcador) {
		this.idmarcador = idmarcador;
	}

	public String getMarcadorUno() {
		return marcadorUno;
	}

	public void setMarcadorUno(String marcadorUno) {
		this.marcadorUno = marcadorUno;
	}

	public String getMarcadorDos() {
		return marcadorDos;
	}

	public void setMarcadorDos(String marcadorDos) {
		this.marcadorDos = marcadorDos;
	}

}
