package co.edu.jerojasol.entities;

import java.io.Serializable;

public class PosicionEquipo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Equipo equipo;
	private int partidosJugados;
	private int partidosGanados;
	private int partidosEmpatados;
	private int partidosPerdidos;
	private int golesFavor;
	private int golesContra;
	private int diferenciaGol;
	private int puntos;
	
	public PosicionEquipo(Equipo equipo) {
		super();
		this.equipo = equipo;
		partidosJugados = 0;
		partidosGanados = 0;
		partidosEmpatados = 0;
		partidosPerdidos = 0;
		golesFavor = 0;
		golesContra = 0;
		diferenciaGol = 0;
		puntos = 0;
	}
	public Equipo getEquipo() {
		return equipo;
	}
	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	public int getPartidosJugados() {
		return partidosJugados;
	}
	public void setPartidosJugados(int partidosJugados) {
		this.partidosJugados = partidosJugados;
	}
	public int getPartidosGanados() {
		return partidosGanados;
	}
	public void setPartidosGanados(int partidosGanados) {
		this.partidosGanados = partidosGanados;
	}
	public int getPartidosEmpatados() {
		return partidosEmpatados;
	}
	public void setPartidosEmpatados(int partidosEmpatados) {
		this.partidosEmpatados = partidosEmpatados;
	}
	public int getPartidosPerdidos() {
		return partidosPerdidos;
	}
	public void setPartidosPerdidos(int partidosPerdidos) {
		this.partidosPerdidos = partidosPerdidos;
	}
	public int getGolesFavor() {
		return golesFavor;
	}
	public void setGolesFavor(int golesFavor) {
		this.golesFavor = golesFavor;
	}
	public int getGolesContra() {
		return golesContra;
	}
	public void setGolesContra(int golesContra) {
		this.golesContra = golesContra;
	}
	public int getDiferenciaGol() {
		return diferenciaGol;
	}
	public void setDiferenciaGol(int diferenciaGol) {
		this.diferenciaGol = diferenciaGol;
	}
	public int getPuntos() {
		return puntos;
	}
	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}
	
}
