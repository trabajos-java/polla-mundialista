/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.jerojasol.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DESARROLLO01
 */
@Entity
@Table(name = "partido")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partido.findAll", query = "SELECT p FROM Partido p")})
public class Partido implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idpartido")
    private Integer idpartido;
    @Column(name = "resultadoUno")
    private Integer resultadoUno;
    @Column(name = "resultadoDos")
    private Integer resultadoDos;
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "idfase", referencedColumnName = "idfase")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Fase idfase;
    @JoinColumn(name = "idequipoDos", referencedColumnName = "idequipo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Equipo idequipoDos;
    @JoinColumn(name = "idequipoUno", referencedColumnName = "idequipo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Equipo idequipoUno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpartido", fetch = FetchType.LAZY)
    private List<Marcador> marcadorList;

    public Partido() {
    }

    public Partido(Integer idpartido) {
        this.idpartido = idpartido;
    }

    public Partido(Integer idpartido, Date fecha) {
        this.idpartido = idpartido;
        this.fecha = fecha;
    }

    public Integer getIdpartido() {
        return idpartido;
    }

    public void setIdpartido(Integer idpartido) {
        this.idpartido = idpartido;
    }

    public Integer getResultadoUno() {
        return resultadoUno;
    }

    public void setResultadoUno(Integer resultadoUno) {
        this.resultadoUno = resultadoUno;
    }

    public Integer getResultadoDos() {
        return resultadoDos;
    }

    public void setResultadoDos(Integer resultadoDos) {
        this.resultadoDos = resultadoDos;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Fase getIdfase() {
        return idfase;
    }

    public void setIdfase(Fase idfase) {
        this.idfase = idfase;
    }

    public Equipo getIdequipoDos() {
        return idequipoDos;
    }

    public void setIdequipoDos(Equipo idequipoDos) {
        this.idequipoDos = idequipoDos;
    }

    public Equipo getIdequipoUno() {
        return idequipoUno;
    }

    public void setIdequipoUno(Equipo idequipoUno) {
        this.idequipoUno = idequipoUno;
    }

    public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@XmlTransient
    public List<Marcador> getMarcadorList() {
        return marcadorList;
    }

    public void setMarcadorList(List<Marcador> marcadorList) {
        this.marcadorList = marcadorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpartido != null ? idpartido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partido)) {
            return false;
        }
        Partido other = (Partido) object;
        if ((this.idpartido == null && other.idpartido != null) || (this.idpartido != null && !this.idpartido.equals(other.idpartido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pollamundilista.Partido[ idpartido=" + idpartido + " ]";
    }
    
}
