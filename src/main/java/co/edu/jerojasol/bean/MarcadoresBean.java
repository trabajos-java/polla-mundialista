package co.edu.jerojasol.bean;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import co.edu.jerojasol.entities.Fase;
import co.edu.jerojasol.entities.Marcador;
import co.edu.jerojasol.entities.MarcadorTemporal;
import co.edu.jerojasol.entities.Partido;
import co.edu.jerojasol.entities.PosicionEquipo;
import co.edu.jerojasol.entities.Usuario;
import co.edu.jerojasol.service.FaseService;
import co.edu.jerojasol.service.MarcadorService;
import co.edu.jerojasol.service.PartidoService;

@ManagedBean(name = "marcadoresBean")
@RequestScoped
public class MarcadoresBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Fase> fases;

	private Map<Integer, Marcador> marcadores;

	private Map<Integer, MarcadorTemporal> marcadoresTemporales;

	private String formatoFecha = "EEEE d 'de' MMMM 'a las' hh:mm aa";

	private String fecha;

	private String pagina;

	List<PosicionEquipo> posicionEquipos;

	@ManagedProperty(value = "#{FaseService}")
	FaseService faseService;

	@ManagedProperty(value = "#{MarcadorService}")
	MarcadorService marcadorService;

	@ManagedProperty(value = "#{PartidoService}")
	PartidoService partidoService;

	@ManagedProperty(value = "#{loginBean}")
	LoginBean loginBean;

	@PostConstruct
	public void init() {

		// if (loginBean.getUsuario() != null){
		fases = faseService.getFases();
		List<Marcador> listaMarcadores = marcadorService
				.consultarMarcadoresPorUsuario(loginBean.getUsuario()
						.getIdusuario());

		marcadores = new HashMap<Integer, Marcador>();

		marcadoresTemporales = new HashMap<Integer, MarcadorTemporal>();

		for (Marcador marcador : listaMarcadores) {
			marcadores.put(marcador.getIdpartido().getIdpartido(), marcador);

			MarcadorTemporal temporal = new MarcadorTemporal();

			if (marcador.getMarcadorUno() == null) {
				temporal.setMarcadorUno("");
				temporal.setMarcadorDos("");
			} else {
				temporal.setMarcadorUno(String.valueOf(marcador
						.getMarcadorUno()));
				temporal.setMarcadorDos(String.valueOf(marcador
						.getMarcadorDos()));
			}

			marcadoresTemporales.put(marcador.getIdpartido().getIdpartido(),
					temporal);
		}
		// }else{
		// FacesContext.getCurrentInstance().getExternalContext().redirect("resultadosMundial.xhtml");
		// }

	}

	public FaseService getFaseService() {
		return faseService;
	}

	public void setFaseService(FaseService faseService) {
		this.faseService = faseService;
	}

	public PartidoService getPartidoService() {
		return partidoService;
	}

	public void setPartidoService(PartidoService partidoService) {
		this.partidoService = partidoService;
	}

	public MarcadorService getMarcadorService() {
		return marcadorService;
	}

	public void setMarcadorService(MarcadorService marcadorService) {
		this.marcadorService = marcadorService;
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public Map<Integer, Marcador> getMarcadores() {
		return marcadores;
	}

	public void setMarcadores(Map<Integer, Marcador> marcadores) {
		this.marcadores = marcadores;
	}

	public Map<Integer, MarcadorTemporal> getMarcadoresTemporales() {
		return marcadoresTemporales;
	}

	public void setMarcadoresTemporales(
			Map<Integer, MarcadorTemporal> marcadoresTemporales) {
		this.marcadoresTemporales = marcadoresTemporales;
	}

	public List<Fase> getFases() {
		return fases;
	}

	public void setFases(List<Fase> fases) {
		this.fases = fases;
	}

	public String getFormatoFecha(Date fechaDate) {
		SimpleDateFormat format = new SimpleDateFormat(formatoFecha,
				new Locale("es", "ES"));

		fecha = format.format(fechaDate);
		return fecha;
	}

	public List<PosicionEquipo> getPosicionEquipos(Fase fase) {

		posicionEquipos = faseService.obtenerPosicionEquipos(fase);
		return posicionEquipos;
	}

	public String getPagina() {
		// pagina = "/pages/marcadores.xhtml";
		pagina = "marcadores";
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public void guardarMarcador(Integer idPartido) {

		FacesMessage msg = null;

		Partido partidoSeleccionado = partidoService
				.consultarPartido(idPartido);

		Calendar ahora = Calendar.getInstance();
		ahora.add(Calendar.HOUR, -1);
		Date fechaActual = ahora.getTime();

		long fechaPartidoMilis = partidoSeleccionado.getFecha().getTime();
		long fechaActualMilis = fechaActual.getTime();

		long diferencia = (fechaPartidoMilis - fechaActualMilis) / (60 * 1000);

		if (diferencia < 10) {
			msg = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Cambio no exitoso",
					"El marcador solo puede modificarse hasta 10 minutos antes de comenzar el partido");
		} else {
			MarcadorTemporal marcadorTemporal = marcadoresTemporales
					.get(idPartido);

			if (marcadorTemporal.getMarcadorUno() == null
					|| marcadorTemporal.getMarcadorDos() == null
					|| marcadorTemporal.getMarcadorUno().equals("")
					|| marcadorTemporal.getMarcadorDos().equals("")) {
				msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Cambio no exitoso", "El marcador es invalido");
			} else {

				int marcadorUno = 0;
				int marcadorDos = 0;
				boolean validado = true;

				try {
					marcadorUno = Integer.parseInt(marcadorTemporal
							.getMarcadorUno());
					marcadorDos = Integer.parseInt(marcadorTemporal
							.getMarcadorDos());
				} catch (NumberFormatException e) {
					validado = false;
				}

				if (validado) {
					marcadores.get(idPartido).setMarcadorUno(marcadorUno);
					marcadores.get(idPartido).setMarcadorDos(marcadorDos);

					Marcador marcadorModificado = marcadorService
							.actualizarMarcador(marcadores.get(idPartido));
					if (marcadorModificado != null) {
						msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
								"Cambio exitoso",
								"El marcador ha sido guardado con exito");
					} else {
						msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Cambio no exitoso",
								"Ha ocurrido un error guardando el marcador");
					}
				} else {
					msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Cambio no exitoso", "El marcador es invalido");
				}

			}
		}

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean validarFecha(Integer idPartido) {
		boolean respuesta = true;

		Partido partidoSeleccionado = partidoService
				.consultarPartido(idPartido);

		Calendar ahora = Calendar.getInstance();
		ahora.add(Calendar.HOUR, -1);
		Date fechaActual = ahora.getTime();

		long fechaPartidoMilis = partidoSeleccionado.getFecha().getTime();
		long fechaActualMilis = fechaActual.getTime();

		long diferencia = (fechaPartidoMilis - fechaActualMilis) / (60 * 1000);

		if (diferencia < 10) {
			respuesta = false;
		}

		return respuesta;
	}

}
