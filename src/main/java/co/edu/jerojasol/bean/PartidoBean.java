package co.edu.jerojasol.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import co.edu.jerojasol.entities.Equipo;
import co.edu.jerojasol.service.EquipoService;

@ManagedBean(name = "partidoBean")
@RequestScoped
public class PartidoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombreEqupio = "";
	private String recurso;

	List<Equipo> equipos;

	@ManagedProperty(value = "#{EquipoService}")
	EquipoService equipoService;

	@PostConstruct
	public void init() {

		equipos = equipoService.getEqupios();

	}

	public String getNombreEqupio() {
		return equipos.get(0).getNombre();
	}

	public void setNombreEqupio(String nombreEqupio) {
		this.nombreEqupio = nombreEqupio;
	}

	public EquipoService getEquipoService() {
		return equipoService;
	}

	public void setEquipoService(EquipoService equipoService) {
		this.equipoService = equipoService;
	}

	public String getRecurso() {
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

}
