package co.edu.jerojasol.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

@ManagedBean(eager=true)
@ApplicationScoped
public class ResourceBean {

	String recurso;
	FacesContext facesContext;
	
	@PostConstruct
	public void init(){
		facesContext = FacesContext.getCurrentInstance();
	}
	
	public String getRecurso() {
		if (recurso == null){
			ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
		            .getExternalContext().getContext();
		recurso = ctx.getRealPath("/");
		}
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public FacesContext getFacesContext() {
		return facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}
	
}
