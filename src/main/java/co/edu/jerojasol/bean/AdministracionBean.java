package co.edu.jerojasol.bean;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import co.edu.jerojasol.entities.Marcador;
import co.edu.jerojasol.entities.Partido;
import co.edu.jerojasol.entities.Usuario;
import co.edu.jerojasol.service.FaseService;
import co.edu.jerojasol.service.MarcadorService;
import co.edu.jerojasol.service.PartidoService;
import co.edu.jerojasol.service.UsuarioService;
import co.edu.jerojasol.util.SendEmail;

@ManagedBean(name = "administracionBean")
@RequestScoped
public class AdministracionBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombres;
	private String apellidos;
	private String login;
	private String password;
	private String password2;
	private String habilitado;
	private String identificacion;
	private String correo;
	
	private String equipoUno;
	private String equipoDos;
	private Integer resultadoUno;
	private Integer resultadoDos;
	
	private Integer idPartido;
	private Integer idPartidoInforme;
	
	private boolean partidoEncontrado;
	
	private List<Marcador> marcadores;
	
	private Partido partido;
    
	@ManagedProperty(value = "#{UsuarioService}")
	UsuarioService usuarioService;
	
	@ManagedProperty(value = "#{FaseService}")
	FaseService faseService;
	
	@ManagedProperty(value = "#{PartidoService}")
	PartidoService partidoService;
	
	@ManagedProperty(value = "#{MarcadorService}")
	MarcadorService marcadorService;

	@PostConstruct
	public void init() {
		partidoEncontrado = Boolean.FALSE;
	}
	
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public FaseService getFaseService() {
		return faseService;
	}

	public void setFaseService(FaseService faseService) {
		this.faseService = faseService;
	}

	public MarcadorService getMarcadorService() {
		return marcadorService;
	}

	public void setMarcadorService(MarcadorService marcadorService) {
		this.marcadorService = marcadorService;
	}

	public PartidoService getPartidoService() {
		return partidoService;
	}

	public void setPartidoService(PartidoService partidoService) {
		this.partidoService = partidoService;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public void createUser() throws NoSuchAlgorithmException{
		
		FacesMessage msg = null;
		boolean valido = true;
		
		System.out.println("Se crea usuario nuevo");
		
		
		if (identificacion.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Identificacion invalida");
			valido = false;
		}
		
		if (nombres.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Nombres invalidos");
			valido = false;
		}
		
		if (apellidos.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Apellidos invalidos");
			valido = false;
		}
		
		if (correo.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Correo invalido");
			valido = false;
		}
		
		if (login.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Login invalido");
			valido = false;
		}
		
		if (password.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Passsord 1 invalido");
			valido = false;
		}
		
		if (password.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Passsord 2 invalido");
			valido = false;
		}
		
		if (!password.equals(password2) && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "Password 1 y Password 2 deben ser iguales");
			valido = false;
		}
		
		if (valido){
			
			Usuario usuarioCreado = usuarioService.createUser(nombres, apellidos, habilitado, login, password, identificacion, correo);
			
			if (usuarioCreado != null){
				msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Creaci�n de Usuario", "El usuario se ha creado exitosamente!");
				setNombres("");
				setApellidos("");
				setLogin("");
				setPassword("");
				setPassword2("");
				setIdentificacion("");
				setCorreo("");
				
			}else{
				msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Creando el Usuario", "El usuario no ha podido ser creado");
			}
		}
		 
        FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Integer getIdPartido() {
		return idPartido;
	}

	public void setIdPartido(Integer idPartido) {
		this.idPartido = idPartido;
	}

	public boolean isPartidoEncontrado() {
		return partidoEncontrado;
	}

	public void setPartidoEncontrado(boolean partidoEncontrado) {
		this.partidoEncontrado = partidoEncontrado;
	}

	public List<Marcador> getMarcadores() {
		return marcadores;
	}

	public void setMarcadores(List<Marcador> marcadores) {
		this.marcadores = marcadores;
	}

	public void consultarMarcadores(){
		System.out.println("Consultando marcadores: "+idPartidoInforme);
		partidoEncontrado=Boolean.TRUE;
		partido = partidoService.consultarPartidoConMarcadores(idPartidoInforme);
		marcadores = partido.getMarcadorList();
		
	}
	
	public void enviarInforme(){
		partidoEncontrado=Boolean.TRUE;
		partido = partidoService.consultarPartidoConMarcadores(idPartidoInforme);
		marcadores = partido.getMarcadorList();
		SendEmail sendEmail = new SendEmail();
		sendEmail.sendFromGMail(partido, marcadores);
		
	}
	
	public void consultarPartido(){
		System.out.println("Consultando marcadores: "+idPartido);
		partidoEncontrado=Boolean.TRUE;
		partido = partidoService.consultarPartidoSinMarcadores(idPartido);
		
	}
	
	public void consultarMarcadoresConId(Integer idPartidoEnviado){
		idPartido= idPartidoEnviado;
		System.out.println("Consultando marcadores: "+idPartido);
		partidoEncontrado=Boolean.TRUE;
		partido = partidoService.consultarPartidoConMarcadores(idPartido);
		marcadores = partido.getMarcadorList();
		
	}
	
	public void cerrarPartido(){
		
		FacesMessage msg = null;
		
		partido = partidoService.consultarPartidoSinMarcadores(idPartido);
		
		partido.setEstado("C");
		partido.setResultadoUno(resultadoUno);
		partido.setResultadoDos(resultadoDos);
		partidoService.actualizarPartido(partido);
		
		if (partidoService != null){
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cerrar Marcador", "El marcador se cerro exitosamente");
		}else{
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cerrar Marcador", "El marcador no se pudo cerrar");
		}
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public String getEquipoUno() {
		if (partido == null){
			equipoUno = "";
		}else{
			equipoUno = partido.getIdequipoUno().getNombre();
		}
		return equipoUno;
	}

	public void setEquipoUno(String equipoUno) {
		this.equipoUno = equipoUno;
	}

	public String getEquipoDos() {
		if (partido == null){
			equipoDos = "";
		}else{
			equipoDos = partido.getIdequipoDos().getNombre();
		}
		return equipoDos;
	}

	public void setEquipoDos(String equipoDos) {
		this.equipoDos = equipoDos;
	}

	public Integer getResultadoUno() {
		if (partido == null){
			resultadoUno = new Integer(0);
		}
		return resultadoUno;
	}

	public void setResultadoUno(Integer resultadoUno) {
		this.resultadoUno = resultadoUno;
	}

	public Integer getResultadoDos() {
		if (partido == null){
			resultadoDos = new Integer(0);
		}
		return resultadoDos;
	}

	public void setResultadoDos(Integer resultadoDos) {
		this.resultadoDos = resultadoDos;
	}

	public Integer getIdPartidoInforme() {
		return idPartidoInforme;
	}

	public void setIdPartidoInforme(Integer idPartidoInforme) {
		this.idPartidoInforme = idPartidoInforme;
	}
	
}
