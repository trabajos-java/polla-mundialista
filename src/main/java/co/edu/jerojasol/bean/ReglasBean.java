package co.edu.jerojasol.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import co.edu.jerojasol.entities.Reglas;

@ManagedBean(name = "reglasBean")
@RequestScoped
public class ReglasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Reglas> reglas;
	
	private Reglas reglaSeleccionada;
	
	@PostConstruct
	public void init(){
		reglas = new ArrayList<Reglas>();
		Reglas reglaFaseGrupos = new Reglas();
		reglaFaseGrupos.setTitulo("Fase de Grupos");
		reglaFaseGrupos.setDescripcion("Por acertar el marcador en cualquiera de los partidos de esta fase se otorgar�n tres puntos.");
		reglas.add(reglaFaseGrupos);
		Reglas reglaOctavosFinal = new Reglas();
		reglaOctavosFinal.setTitulo("Octavos de Final");
		reglaOctavosFinal.setDescripcion("Por acertar el marcador en cualquiera de los partidos de esta fase se otorgar�n cuatro puntos.");
		reglas.add(reglaOctavosFinal);
		Reglas reglaCuartosFinal = new Reglas();
		reglaCuartosFinal.setTitulo("Cuartos de Final");
		reglaCuartosFinal.setDescripcion("Por acertar el marcador en cualquiera de los partidos de esta fase se otorgar�n cinco puntos.");
		reglas.add(reglaCuartosFinal);
		Reglas reglaSemiFinal = new Reglas();
		reglaSemiFinal.setTitulo("SemiFinal");
		reglaSemiFinal.setDescripcion("Por acertar el marcador en cualquiera de los partidos de esta fase se otorgar�n seis puntos.");
		reglas.add(reglaSemiFinal);
		Reglas reglaTerceroCuarto = new Reglas();
		reglaTerceroCuarto.setTitulo("Tercero y Cuarto");
		reglaTerceroCuarto.setDescripcion("Por acertar el marcador en este partido se otorgar�n siete puntos.");
		reglas.add(reglaTerceroCuarto);
		Reglas reglaFinal = new Reglas();
		reglaFinal.setTitulo("Final");
		reglaFinal.setDescripcion("Por acertar el marcador en este partido se otorgar�n ocho puntos");
		reglas.add(reglaFinal);
		Reglas reglaBonusUno = new Reglas();
		reglaBonusUno.setTitulo("Bono Uno");
		reglaBonusUno.setDescripcion("Por acertar quien gano el partido o si este termino en empate, se otorgaran dos puntos, en caso de haber acertado en el marcador exacto, este bono no se har� efectivo.");
		reglas.add(reglaBonusUno);
		Reglas reglaBonusDos = new Reglas();
		reglaBonusDos.setTitulo("Bono Dos");
		reglaBonusDos.setDescripcion("Por acertar en el marcador de alguno de los equipos, se otorgara un punto, en caso de haber acertado en el marcador exacto, o ganar el bono dos, este bono no se har� efectivo.");
		reglas.add(reglaBonusDos);
	}

	public List<Reglas> getReglas() {
		return reglas;
	}

	public void setReglas(List<Reglas> reglas) {
		this.reglas = reglas;
	}

	public Reglas getReglaSeleccionada() {
		return reglaSeleccionada;
	}

	public void setReglaSeleccionada(Reglas reglaSeleccionada) {
		this.reglaSeleccionada = reglaSeleccionada;
	}

}
