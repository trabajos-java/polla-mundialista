package co.edu.jerojasol.bean;
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
 
import java.io.Serializable;
 
@ManagedBean
@SessionScoped
public class PageController implements Serializable {
 
	private static final long serialVersionUID = 1L;

	private String processPage1;
	private String processPage2;

	public String getProcessPage1() {
		processPage1 = "success";
		return processPage1;
	}

	public void setProcessPage1(String processPage1) {
		this.processPage1 = processPage1;
	}

	public String getProcessPage2() {
		processPage2 = "success";
		return processPage2;
	}

	public void setProcessPage2(String processPage2) {
		this.processPage2 = processPage2;
	}
	
}