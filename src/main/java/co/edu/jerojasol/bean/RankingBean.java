package co.edu.jerojasol.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import co.edu.jerojasol.entities.Fase;
import co.edu.jerojasol.entities.Marcador;
import co.edu.jerojasol.entities.MarcadorTemporal;
import co.edu.jerojasol.entities.PosicionUsuario;
import co.edu.jerojasol.entities.Usuario;
import co.edu.jerojasol.service.FaseService;
import co.edu.jerojasol.service.MarcadorService;
import co.edu.jerojasol.service.UsuarioService;

@ManagedBean(name = "rankingBean")
@RequestScoped
public class RankingBean implements Serializable {

	private static final long serialVersionUID = 1L;

	List<PosicionUsuario> listaPosicionUsuario;
	
	private List<Fase> fases;
	
	Usuario usuarioSeleccionado;
	
	private Map<Integer, Marcador> marcadores;
	
	@ManagedProperty(value = "#{UsuarioService}")
	UsuarioService usuarioService;
	
	@ManagedProperty(value = "#{FaseService}")
	FaseService faseService;
	
	@ManagedProperty(value = "#{MarcadorService}")
	MarcadorService marcadorService;

	@PostConstruct
	public void init() {
		listaPosicionUsuario = usuarioService.obtenerPosicionUsuarios();
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public List<PosicionUsuario> getListaPosicionUsuario() {
		return listaPosicionUsuario;
	}

	public void setListaPosicionUsuario(List<PosicionUsuario> listaPosicionUsuario) {
		this.listaPosicionUsuario = listaPosicionUsuario;
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
		cargarFasesPorUsuario();
	}

	public List<Fase> getFases() {
		return fases;
	}

	public void setFases(List<Fase> fases) {
		this.fases = fases;
	}

	public FaseService getFaseService() {
		return faseService;
	}

	public void setFaseService(FaseService faseService) {
		this.faseService = faseService;
	}
	
	public MarcadorService getMarcadorService() {
		return marcadorService;
	}

	public void setMarcadorService(MarcadorService marcadorService) {
		this.marcadorService = marcadorService;
	}

	public Map<Integer, Marcador> getMarcadores() {
		return marcadores;
	}

	public void setMarcadores(Map<Integer, Marcador> marcadores) {
		this.marcadores = marcadores;
	}

	public void cargarFasesPorUsuario(){
		fases = faseService.getFases();
		List<Marcador> listaMarcadores = marcadorService
				.consultarMarcadoresPorUsuario(usuarioSeleccionado.getIdusuario());

		marcadores = new HashMap<Integer, Marcador>();

		for (Marcador marcador : listaMarcadores) {
			marcadores.put(marcador.getIdpartido().getIdpartido(), marcador);

		}
	}

}
