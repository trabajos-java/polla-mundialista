package co.edu.jerojasol.bean;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import co.edu.jerojasol.entities.Usuario;
import co.edu.jerojasol.service.UsuarioService;

@ManagedBean(name = "perfilBean")
@RequestScoped
public class PerfilBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String password;
	private String password1;
	private String password2;
	
	@ManagedProperty(value = "#{UsuarioService}")
	UsuarioService usuarioService;
	
	@ManagedProperty(value = "#{loginBean}")
	LoginBean loginBean;
	
	@PostConstruct
	public void init() {
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public void cambiarContrasena(){
		
		FacesMessage msg = null;
		boolean valido = true;
		
		if (password.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error modificando la contraseña", "Contraseña actual invalida");
			valido = false;
		}
		
		try {
			if (!usuarioService.obtenerMD5(password).equals(loginBean.getUsuario().getPassword()) && valido){
				msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error modificando la contraseña", "Contraseña actual invalida");
				valido = false;
			}
		} catch (NoSuchAlgorithmException e) {
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error modificando la contraseña", "Contraseña actual invalida");
			valido = false;
		}
		
		if (password1.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error modificando la contraseña", "Contraseña nueva invalida");
			valido = false;
		}
		
		if (password2.equals("") && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error modificando la contraseña", "Contraseña nueva invalida");
			valido = false;
		}
		
		if (!password1.equals(password2) && valido){
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error modificando la contraseña", "La confirmación de la contraseña nueva es erronea");
			valido = false;
		}
		
		if (valido){
			
			try {
				Usuario usuarioAActualizar = loginBean.getUsuario();
				usuarioAActualizar.setPassword(usuarioService.obtenerMD5(password1));
				Usuario usuarioActualizado = usuarioService.actualizarUsuario(usuarioAActualizar);
				
				if (usuarioActualizado != null){
					msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Modificación de la contraseña", "La contraseña se ha modificado exitosamente!");
					setPassword("");
					setPassword1("");
					setPassword2("");
				}
				
			} catch (NoSuchAlgorithmException e) {
				msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error modificando la contraseña", "Contraseña actual invalida");
			}
			
		}
		FacesContext.getCurrentInstance().addMessage(null, msg);
		
	}
	
}
