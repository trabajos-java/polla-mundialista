package co.edu.jerojasol.bean;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import co.edu.jerojasol.entities.Usuario;
import co.edu.jerojasol.service.UsuarioService;

@ManagedBean()
@SessionScoped
public class LoginBean {

	private String username;
	
	private String password;
	
	private Usuario usuario;
	
	private boolean administrador = false;
	
	public boolean loggedIn = false;
	
	@ManagedProperty(value = "#{UsuarioService}")
	UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void inactividad(){
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
                "Sin actividad.", "Se ha perdido la sesion por inactividad"));
	}

	public void login(ActionEvent actionEvent) throws NoSuchAlgorithmException {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage msg = null;

		if (username != null && password != null) {

			Usuario user = usuarioService.consultarUsuarioPorLoginYPassword(username, password);

			if (user != null) {
				loggedIn = true;
				
				if (username.equals("admin")){
					administrador=true;
				}
				
				msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", user.getNombres()+" "+user.getApellidos());

				setUsuario(user);
			}else {
				loggedIn = false;
				msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error", "Login o password incorrectos");
			}
		} else {
			loggedIn = false;
			msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error", "Login o password incorrectos");
		}

		FacesContext.getCurrentInstance().addMessage(null, msg);
		context.addCallbackParam("loggedIn", loggedIn);
	}
	
	public void logout(ActionEvent actionEvent) throws IOException {

		if (loggedIn) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Logout", "Se ha cerrado la sesi�n con �xito");

			setUsuario(null);

			FacesContext.getCurrentInstance().addMessage(null, msg);
			loggedIn = false;
			administrador = false;
			
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect("resultadosMundial.xhtml");
		}

	}
	
	public void logoutInactividad() throws IOException {
		
		if (loggedIn) {
			
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
	                "Sin actividad.", "Se ha perdido la sesion por inactividad"));
			
			setUsuario(null);
			
			loggedIn = false;
			administrador = false;
			
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect("resultadosMundial.xhtml");
		}
		
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isAdministrador() {
		return administrador;
	}

	public void setAdministrador(boolean administrador) {
		this.administrador = administrador;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

}
