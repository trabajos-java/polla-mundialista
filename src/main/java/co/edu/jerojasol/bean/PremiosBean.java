package co.edu.jerojasol.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import co.edu.jerojasol.entities.Usuario;
import co.edu.jerojasol.service.UsuarioService;

@ManagedBean(name = "premiosBean")
@RequestScoped
public class PremiosBean {

	private String pozo;
	private String pozoSegundo;

	@ManagedProperty(value = "#{UsuarioService}")
	UsuarioService usuarioService;

	@PostConstruct
	public void init() {
		List<Usuario> usuarios = usuarioService.getUsuarios();

		Double valorPozo = (usuarios.size() * 50000)
				- (usuarios.size() * 50000 * 0.1);
		
		Double pozoUno = valorPozo*0.7;
		Double pozoDos = valorPozo*0.3;
		
		pozo = String.valueOf(pozoUno.intValue());
		pozoSegundo = String.valueOf(pozoDos.intValue());

	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public String getPozo() {
		return pozo;
	}

	public void setPozo(String pozo) {
		this.pozo = pozo;
	}

	public String getPozoSegundo() {
		return pozoSegundo;
	}

	public void setPozoSegundo(String pozoSegundo) {
		this.pozoSegundo = pozoSegundo;
	}

}
