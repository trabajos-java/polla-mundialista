package co.edu.jerojasol.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import co.edu.jerojasol.entities.Fase;
import co.edu.jerojasol.entities.Partido;
import co.edu.jerojasol.entities.PosicionEquipo;
import co.edu.jerojasol.service.FaseService;
import co.edu.jerojasol.service.PartidoService;

@ManagedBean(name = "resultadosMundialBean")
@RequestScoped
public class ResultadosMundialBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Fase> fases;

	private String formatoFecha = "EEEE d 'de' MMMM 'a las' hh:mm aa";

	private String formatoFechaPartidos = "EEEE d 'de' MMMM";
	
	private String formatoHoraPartidos = "hh:mm aa";

	private String fecha;

	private String pagina;

	private Map<String, List<Partido>> partidosFecha;
	
	private List<String> fechas;
	
	List<PosicionEquipo> posicionEquipos;

	@ManagedProperty(value = "#{FaseService}")
	FaseService faseService;

	@ManagedProperty(value = "#{PartidoService}")
	PartidoService partidoService;

	@PostConstruct
	public void init() {
		
		fases = faseService.getFases();
		
		List<Partido> partidosPorFecha = partidoService
				.obtenerPartidosPorFecha();

		SimpleDateFormat format = new SimpleDateFormat(formatoFechaPartidos,
				new Locale("es", "ES"));

		int contadorDias = 0;

		partidosFecha = new HashMap<String, List<Partido>>();
		fechas = new ArrayList<String>();

		for (Partido partido : partidosPorFecha) {
			String fecha = format.format(partido.getFecha());

			List<Partido> partidosTmp = (List<Partido>) partidosFecha
					.get(fecha);

			if (partidosTmp == null) {
				contadorDias++;
				if (contadorDias<4){
					partidosTmp = new ArrayList<Partido>();
					fechas.add(fecha);
				}
			}
			if (contadorDias<4){
				partidosTmp.add(partido);
				partidosFecha.put(fecha, partidosTmp);
			}
			if (contadorDias==4){
				break;
			}

		}

	}

	public PartidoService getPartidoService() {
		return partidoService;
	}

	public void setPartidoService(PartidoService partidoService) {
		this.partidoService = partidoService;
	}

	public FaseService getFaseService() {
		return faseService;
	}

	public void setFaseService(FaseService faseService) {
		this.faseService = faseService;
	}

	public List<Fase> getFases() {
		return fases;
	}

	public void setFases(List<Fase> fases) {
		this.fases = fases;
	}

	public String getFormatoFecha(Date fechaDate) {
		SimpleDateFormat format = new SimpleDateFormat(formatoFecha,
				new Locale("es", "ES"));

		fecha = format.format(fechaDate);
		return fecha;
	}
	
	public String getFormatoHora(Date fechaDate) {
		SimpleDateFormat format = new SimpleDateFormat(formatoHoraPartidos,
				new Locale("es", "ES"));
		
		fecha = format.format(fechaDate);
		return fecha;
	}

	public List<PosicionEquipo> getPosicionEquipos(Fase fase) {

		posicionEquipos = faseService.obtenerPosicionEquipos(fase);
		return posicionEquipos;
	}

	public String getPagina() {
		// pagina = "/pages/marcadores.xhtml";
		pagina = "marcadores";
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public String obtenerResultadoUno(Partido partido) {
		String resultado = "";

		if (partido.getEstado().equals("C")) {
			resultado = String.valueOf(partido.getResultadoUno());
		}
		return resultado;
	}

	public String obtenerResultadoDos(Partido partido) {
		String resultado = "";

		if (partido.getEstado().equals("C")) {
			resultado = String.valueOf(partido.getResultadoDos());
		}
		return resultado;
	}

	public Map<String, List<Partido>> getPartidosFecha() {
		return partidosFecha;
	}

	public void setPartidosFecha(Map<String, List<Partido>> partidosFecha) {
		this.partidosFecha = partidosFecha;
	}

	public List<String> getFechas() {
		return fechas;
	}

	public void setFechas(List<String> fechas) {
		this.fechas = fechas;
	}
	
}
