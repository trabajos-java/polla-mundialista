<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

        <!-- Created on: 01/12/2011 -->
        <head>

                <meta http-equiv="Content-Type" content="text/html; charset=us-ascii">
                <meta name="ProgId" content="Word.Document">
                <meta name="Generator" content="Microsoft Word 12">
                <meta name="Originator" content="Microsoft Word 12">
                <title>Marcadores</title>
				<style>
				
						table {
						   width: 60%;
						   border: 1px solid #000;
						}
						th, td {
						   width: 15%;
						   text-align: center;
						   vertical-align: top;
						   border: 1px solid #000;
						   border-collapse: collapse;
						   padding: 0.3em;
						   caption-side: bottom;
						}
						caption {
						   padding: 0.3em;
						   color: #0000FF;
							background: #000;
						}
						th {
						   background: #eee;
						}
				</style>		
                
        </head>
        <body>
        
        <br/>
						
						A continuaci&oacute;n los marcadores que colocaron los usuarios para el partido ${partido.idequipoUno.nombre} - ${partido.idequipoDos.nombre}, ${partido.idfase.nombre}.

						<br/>
						<br/>
						<br/>
						
                        <div align="center">
						
						<table>
							<thead>
								<tr>
									<th scope="col">Usuario</th>
									<th scope="col">${partido.idequipoUno.nombre}</th>
									<th scope="col">${partido.idequipoDos.nombre}</th>
								</tr>
							</thead>
							<tbody>
							
							<#list marcadores as marcador>
								<tr>
									<th>${marcador.idusuario.nombres} ${marcador.idusuario.apellidos}</th>
									<td>${marcador.marcadorUno}</td>
									<td>${marcador.marcadorDos}</td>
								</tr>
							</#list>
							</tbody>
						</table>
						
                </div>

        </body>

</html>